#version 330 core
in vec3 ourColor;
in vec2 ourTexCoord;


uniform sampler2D texture1;
uniform sampler2D texture2;
out vec4 color;

void main()
{
    //color = vec4(ourColor, 1.0f);
	//color = ourColor;
    color = texture(texture1,ourTexCoord) * texture(texture2,ourTexCoord);
}