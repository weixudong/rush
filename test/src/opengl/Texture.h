#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

class Texture
{
public:

	GLuint texture;
	GLuint samplerIndex;

	Texture(const GLchar* texturePath) {


		//unsigned int texture;
		glGenTextures(1, &this->texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		// 为当前绑定的纹理对象设置环绕、过滤方式
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// 加载并生成纹理
		int width, height, nrChannels;
		GLint format = GL_RGB;
		stbi_set_flip_vertically_on_load(true);
		unsigned char *data = stbi_load(texturePath, &width, &height, &nrChannels, 0);
		if (data)
		{
			if (nrChannels == 4) {
				format = GL_RGBA;
			}
			glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);

		//glUniform1i(glGetUniformLocation(ourShader.ID, "texture1"), 0);
	}
	void BindShader(GLuint shaderId, const GLchar * samplerName, GLuint index) {
		this->samplerIndex = index;
		glUniform1i(glGetUniformLocation(shaderId, samplerName), index);
	}
	void Use() {
		glActiveTexture(GL_TEXTURE0+this->samplerIndex);
		glBindTexture(GL_TEXTURE_2D, this->texture);
	}





};

#endif //TEXTURE_H
