//  Hello World server

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include "zhelpers.h"

int main (void)
{

     int major, minor, patch;
    zmq_version (&major, &minor, &patch);
    printf ("当前ZMQ版本号为 %d.%d.%d\n", major, minor, patch);

    //  Socket to talk to clients
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://*:5555");
    assert (rc == 0);

    while (1) {
        char * buffer = s_recv(responder);

        //char buffer [100];
       // zmq_recv (responder, buffer, 100, 0);
        printf ("Received Hello %s-%ld \n",buffer,strlen(buffer));
        sleep (1);          //  Do some 'work'
        s_send (responder, buffer);
        free(buffer);
    }
    return 0;
}