//  Hello World client
#include <zmq.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "zhelpers.h"
int main (void)
{
    printf ("Connecting to hello world server...\n");
    void *context = zmq_ctx_new ();
    void *requester = zmq_socket (context, ZMQ_REQ);
    zmq_connect (requester, "tcp://localhost:5555");

    int request_nbr;
    int i=0;
    for (request_nbr = 0; request_nbr != 100; request_nbr++) {
        //char buffer [10];
        char sendbuffer [10];
        sprintf(sendbuffer,"c:%d",i++);
        printf ("Sending  %s...\n", sendbuffer);
        s_send(requester,sendbuffer);
        char * buffer = s_recv(requester);
        //zmq_send (requester, sendbuffer, strlen(sendbuffer), 0);
       // zmq_recv (requester, buffer, 10, 0);
        printf ("Received  %s\n", buffer);
        free(buffer);
    }
    zmq_close (requester);
    zmq_ctx_destroy (context);
    return 0;
}