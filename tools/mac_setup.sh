#!/bin/bash -e
brew install capnp \
             czmq \
             coreutils \
             eigen \
             ffmpeg \
             glfw \
             glew \
             libarchive \
             libusb \
             libtool \
             llvm \
             pyenv \
             qt5 \
             zeromq